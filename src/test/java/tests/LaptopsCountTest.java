package tests;

import org.junit.Test;

public class LaptopsCountTest extends BaseYandexTest {

    /*
    * На Yandex.Market получить информацию о кол-ве моделей ноутбуков от производителей Apple, Lenovo, Xiaomi.
    * Посчитать цифры раздельно для процессоров Core i7 и Core i5.
    * Результат вывести в следующем виде:
    * core i7:
    * apple     3
    * lenovo  15
    * xiaomi   8
    *
    * core i5:
    * apple     5
    * lenovo   12
    * xiaomi   11
    * */
    @Test
    public void testLaptopsCount() {
        app.selectLaptops();

        System.out.println("i7:");
        app.selectFilter("Процессор", "Core i7");
        app.selectFilter("Производитель", "Apple");
        System.out.println("Apple:\t" + app.countForCurrentFilter());
        app.selectFilter("Производитель", "Apple");
        app.selectFilter("Производитель", "Lenovo");
        System.out.println("Lenovo:\t" + app.countForCurrentFilter());
        app.selectFilter("Производитель", "Lenovo");
        app.selectFilter("Производитель", "Xiaomi");
        System.out.println("Xiaomi:\t" + app.countForCurrentFilter());
        app.selectFilter("Производитель", "Xiaomi");
        app.selectFilter("Процессор", "Core i7");

        System.out.println("\ni5:");
        app.selectFilter("Процессор", "Core i5");
        app.selectFilter("Производитель", "Apple");
        System.out.println("Apple:\t" + app.countForCurrentFilter());
        app.selectFilter("Производитель", "Apple");
        app.selectFilter("Производитель", "Lenovo");
        System.out.println("Lenovo:\t" + app.countForCurrentFilter());
        app.selectFilter("Производитель", "Lenovo");
        app.selectFilter("Производитель", "Xiaomi");
        System.out.println("Xiaomi:\t" + app.countForCurrentFilter());

    }
}
