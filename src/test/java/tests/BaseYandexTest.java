package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.UserActions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static utils.DriverRunner.*;
import static utils.DriverRunner.getDriver;


public class BaseYandexTest {
    UserActions app = new UserActions();

    private Properties loadProps() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("src/test/resources/config.properties");
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }

    @Before
    public void setUp() {
        Properties prop = loadProps();
        System.setProperty("webdriver.chrome.driver", prop.getProperty("driver.path"));
        WebDriver wd = new ChromeDriver();
        setDriver(wd);
        getDriver().manage().window().maximize();
        getDriver().get("https://yandex.ru/");
        getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @After
    public void shutDown() {
        getDriver().quit();
    }
}
