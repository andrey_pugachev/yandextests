package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverRunner;

import java.util.List;

import static utils.DriverRunner.*;

public class YandexMainPage {
    public static WebElement getHorizontalMainMenuElement(String elementText) {
        return getDriver().findElement(By.xpath("//div[contains(@class,'home-tabs')]//a[contains(text(), '"
                + elementText + "')]]"));
    }

    public static WebElement getSearchInput() {
        return  getDriver().findElement(By.cssSelector("#text"));
    }

    public static List<WebElement> getPopupContent() {
        return getDriver().findElements(By.xpath("//li[contains(@class,'i-bem')]"));
    }

    public static void waitWhilePopupBeVisible() {
        new WebDriverWait(DriverRunner.getDriver(), 15).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(
                "//div[@class = 'popup__content']")));
    }
}
