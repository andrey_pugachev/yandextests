package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.DriverRunner;

public class YandexMarketMainPage {

    public static WebElement getHorizontalMenuElement(String elementText) {
        return DriverRunner.getDriver().findElement(By.xpath("//div[contains(@class,'tabs-container')]//a[span" +
                "[contains(text(), '" + elementText + "')]]"));
    }
}
