package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverRunner;

public class YandexCompTechPage {
    public static WebElement getLnikByText(String textLink) {
        return DriverRunner.getDriver().findElement(By.xpath("//a[contains(text(),'" + textLink + "')]"));
    }

    public static WebElement getFilterCheckboxByText(String section, String text) {
        return DriverRunner.getDriver().findElement(By.xpath("//fieldset[legend[contains(text(), '" + section +
                "')]]//span[contains(text(), '" + text + "')]"));
    }

    public static void waitWhileFiltering() {
        new WebDriverWait (DriverRunner.getDriver(), 15).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(
                "//*[contains(@class, 'preloadable__paranja')]")));
    }

    public static WebElement getPagination() {
        return DriverRunner.getDriver().findElement(By.xpath("//div[contains(@class,'n-pager_js')]"));
    }
}
