package utils;

import org.openqa.selenium.WebDriver;

public class DriverRunner {
    private static WebDriver driver;

    private DriverRunner() {
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        DriverRunner.driver = driver;
    }
}
