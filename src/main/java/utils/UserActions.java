package utils;

import org.json.JSONObject;
import org.openqa.selenium.WebElement;
import pages.YandexCompTechPage;
import pages.YandexMainPage;
import pages.YandexMarketMainPage;

public class UserActions {
    public String getPopupText(int n) {
        return YandexMainPage.getPopupContent().get(n-1).getAttribute("textContent")
                .replaceAll("Быстрый ответ:","");
    }

    public void typeToSearchInput(String text) {
        YandexMainPage.getSearchInput().click();
        YandexMainPage.getSearchInput().sendKeys(text);
    }

    public void selectLaptops() {
        YandexMainPage.getHorizontalMainMenuElement("Маркет");
        YandexMarketMainPage.getHorizontalMenuElement("Компьютерная техника").click();
        YandexCompTechPage.getLnikByText("Ноутбуки").click();
    }

    public void selectFilter(String section, String filter) {
        YandexCompTechPage.getFilterCheckboxByText(section, filter).click();
        YandexCompTechPage.waitWhileFiltering();
    }

    public String countForCurrentFilter() {
        return "" + new JSONObject(YandexCompTechPage.getPagination().getAttribute("data-bem"))
                .getJSONObject("n-pager").get("total");
    }
}

